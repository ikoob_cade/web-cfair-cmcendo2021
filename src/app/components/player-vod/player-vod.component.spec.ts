import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerVodComponent } from './player-vod.component';

describe('PlayerVodComponent', () => {
  let component: PlayerVodComponent;
  let fixture: ComponentFixture<PlayerVodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerVodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerVodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
