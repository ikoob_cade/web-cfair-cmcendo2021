import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotSupportIeComponent } from './not-support-ie.component';

describe('NotSupportIeComponent', () => {
  let component: NotSupportIeComponent;
  let fixture: ComponentFixture<NotSupportIeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotSupportIeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotSupportIeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
