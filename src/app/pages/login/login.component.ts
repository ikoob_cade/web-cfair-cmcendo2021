import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UAParser } from 'ua-parser-js';

enum AUTH_ERROR_MESSAGE {
  UNAUTHORIZED = 'Login information does not match.',
  NOT_FOUND = 'E-mail could not be found.',
  ENTER_ALL = 'Please enter all values.',
  IMPOSSIBLE_TIME = '로그인은 7월 25일 (일) 오전 8시부터 가능합니다.'
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  private user: any;
  public passwordError = ''; // 로그인 에러메세지
  public member = {
    email: '',
    password: '',
  };

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {
  }

  login(): void {
    if (this.loginValidator()) {
      const parser = new UAParser();
      const fullUserAgent = parser.getResult();

      const body: any = {
        email: this.member.email,
        password: this.member.password,
        userAgent: JSON.stringify(fullUserAgent),
        browser: JSON.stringify(fullUserAgent.browser),
        device: JSON.stringify(fullUserAgent.device),
        engine: JSON.stringify(fullUserAgent.engine),
        os: JSON.stringify(fullUserAgent.os),
        ua: JSON.stringify(fullUserAgent.ua),
      };

      this.auth.login(body).subscribe(async res => {
        if (res.token) {
          this.user = res;
          this.passwordError = '';
          // this.policyAlertBtn.nativeElement.click();

          this.goToMain();
        }
      }, error => {
        if (error.status === 401) {
          this.passwordError = AUTH_ERROR_MESSAGE.UNAUTHORIZED;
        }
        if (error.status === 404) {
          this.passwordError = AUTH_ERROR_MESSAGE.NOT_FOUND;
        }
        if (error.status === 406){
          this.passwordError = AUTH_ERROR_MESSAGE.IMPOSSIBLE_TIME;
        }
        
      });
    } else {
      alert(AUTH_ERROR_MESSAGE.ENTER_ALL);
    }
  }

  loginValidator(): boolean {
    if (!this.member.email || !this.member.password) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    sessionStorage.setItem('cfair', JSON.stringify(this.user));
    // sessionStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }
}
