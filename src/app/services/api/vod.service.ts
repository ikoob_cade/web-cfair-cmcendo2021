import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class VodService {
  private serverUrl = '/events/:eventId/vod';

  constructor(private http: HttpClient) { }

  find(): Observable<any> {
    return this.http.get(this.serverUrl)
      .pipe(catchError(this.handleError));
  }

  findOne(vodId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + vodId)
      .pipe(catchError(this.handleError));
  }

  // ! 댓글
  findComments(posterId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + posterId + '/comment')
      .pipe(catchError(this.handleError));
  }

  createComment(posterId: string, body: any): Observable<any> {
    return this.http.post(this.serverUrl + '/' + posterId + '/comment', body)
      .pipe(catchError(this.handleError));
  }

  updateComment(posterId: string, commentId: string, body: any): Observable<any> {
    return this.http.put(this.serverUrl + '/' + posterId + '/comment/' + commentId, body)
      .pipe(catchError(this.handleError));
  }

  deleteComment(posterId: string, commentId: string): Observable<any> {
    return this.http.delete(this.serverUrl + '/' + posterId + '/comment/' + commentId)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}
